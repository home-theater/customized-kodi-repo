#!/usr/bin/env bash

set -e

function clean {
  if [ -e common.d ]; then rm common.d; fi
  if [ -e create_repository.d ]; then rm create_repository.d; fi
}

clean

curl -Ssf https://codeberg.org/home-theater/scripts/raw/branch/master/common.d -o common.d
curl -Ssf https://codeberg.org/home-theater/scripts/raw/branch/master/create_repository.d -o create_repository.d
chmod +x create_repository.d

./create_repository.d

clean