module config;

import common : GitPath;

string repo_addon_id = "repository.home-theater-customized";

GitPath[] template_engine_addon_git_paths = [
  GitPath("https://codeberg.org/home-theater/customized-kodi-repo_addon"),
  GitPath("https://codeberg.org/home-theater/kodi-addon_recent-dl-info")
];

GitPath[] regular_addon_git_paths = [
  GitPath("https://codeberg.org/home-theater/codeberg-kodi-repo_addon", "/repository.home-theater-codeberg/")
];

// string template_engine_config = `
// {
//   "defines": [],
//   "substitutions": {
//     "baseurl": "https://home-theater.example.com",
//     "password": "example"
//   }
// }
// `;
